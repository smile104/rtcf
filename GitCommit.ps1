git config --global user.name "toshinori takahashi" > GitLog.txt
git config --global user.email "t.takahashi03@espg.jp" >> GitLog.txt

git init >> GitLog.txt
git add . >> GitLog.txt
git commit -m "Initial commit" >> GitLog.txt
git remote add origin/main https://gitlab.com/smile104/rtcf.git >> GitLog.txt
git push -u origin main >> GitLog.txt
git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(bold yellow)%d%C(reset)' --all >> GitLog.txt